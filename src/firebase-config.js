import { initializeApp } from "firebase/app";

const firebaseConfig = {
  apiKey: "AIzaSyAtyBtU5H8vWkBuRDIG3NYztWmKu8aTRQQ",
  authDomain: "authproject-21c82.firebaseapp.com",
  projectId: "authproject-21c82",
  storageBucket: "authproject-21c82.appspot.com",
  messagingSenderId: "386164732148",
  appId: "1:386164732148:web:b23265cf1c520c43f9aea1",
  measurementId: "G-Y2SMTFRCP4"
};

const app = initializeApp(firebaseConfig);
export default firebaseConfig;